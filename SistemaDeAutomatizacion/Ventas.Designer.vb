﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CedulaLabel As System.Windows.Forms.Label
        Dim TelefonoLabel As System.Windows.Forms.Label
        Dim ApellidoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim COD_UnidadLabel As System.Windows.Forms.Label
        Dim Numero_MatriculaLabel As System.Windows.Forms.Label
        Dim NUmero_AcientosLabel As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label()
        Me.DestinosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TaxiDBDataConfig = New SistemaDeAutomatizacion.TaxiDBDataConfig()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cedula = New System.Windows.Forms.Label()
        Me.apellido = New System.Windows.Forms.Label()
        Me.nombre = New System.Windows.Forms.Label()
        Me.telefono = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.fecha_actual = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbl_codcli = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.NUmero_AcientosTextBox = New System.Windows.Forms.TextBox()
        Me.unidades = New System.Windows.Forms.BindingSource(Me.components)
        Me.Numero_MatriculaTextBox = New System.Windows.Forms.TextBox()
        Me.COD_UnidadTextBox = New System.Windows.Forms.TextBox()
        Me.FotoPictureBox = New System.Windows.Forms.PictureBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Lista = New System.Windows.Forms.ComboBox()
        Me.hora_actual = New System.Windows.Forms.Label()
        Me.DestinosTableAdapter = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.DestinosTableAdapter()
        Me.hora = New System.Windows.Forms.Timer(Me.components)
        Me.vender = New System.Windows.Forms.Button()
        Me.cancel = New System.Windows.Forms.Button()
        Me.progreso = New System.Windows.Forms.ProgressBar()
        Me.TableAdapterManager = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.TableAdapterManager()
        Me.BoletoTableAdapter1 = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.BoletoTableAdapter()
        Me.UnidadesTableAdapter = New SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.UnidadesTableAdapter()
        CedulaLabel = New System.Windows.Forms.Label()
        TelefonoLabel = New System.Windows.Forms.Label()
        ApellidoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        COD_UnidadLabel = New System.Windows.Forms.Label()
        Numero_MatriculaLabel = New System.Windows.Forms.Label()
        NUmero_AcientosLabel = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        CType(Me.DestinosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TaxiDBDataConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.unidades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FotoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CedulaLabel
        '
        CedulaLabel.AutoSize = True
        CedulaLabel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CedulaLabel.Location = New System.Drawing.Point(15, 22)
        CedulaLabel.Name = "CedulaLabel"
        CedulaLabel.Size = New System.Drawing.Size(62, 18)
        CedulaLabel.TabIndex = 6
        CedulaLabel.Text = "Cedula:"
        '
        'TelefonoLabel
        '
        TelefonoLabel.AutoSize = True
        TelefonoLabel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TelefonoLabel.Location = New System.Drawing.Point(15, 100)
        TelefonoLabel.Name = "TelefonoLabel"
        TelefonoLabel.Size = New System.Drawing.Size(70, 18)
        TelefonoLabel.TabIndex = 12
        TelefonoLabel.Text = "Telefono:"
        '
        'ApellidoLabel
        '
        ApellidoLabel.AutoSize = True
        ApellidoLabel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ApellidoLabel.Location = New System.Drawing.Point(15, 48)
        ApellidoLabel.Name = "ApellidoLabel"
        ApellidoLabel.Size = New System.Drawing.Size(69, 18)
        ApellidoLabel.TabIndex = 8
        ApellidoLabel.Text = "Apellido:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(15, 74)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(68, 18)
        NombreLabel.TabIndex = 10
        NombreLabel.Text = "Nombre:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.Location = New System.Drawing.Point(20, 33)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(0, 16)
        Label5.TabIndex = 16
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Label1.Location = New System.Drawing.Point(8, 19)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(146, 20)
        Label1.TabIndex = 27
        Label1.Text = "Seleccione Destino"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Label2.Location = New System.Drawing.Point(8, 102)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(142, 20)
        Label2.TabIndex = 28
        Label2.Text = "Seleccione Unidad"
        '
        'COD_UnidadLabel
        '
        COD_UnidadLabel.AutoSize = True
        COD_UnidadLabel.Location = New System.Drawing.Point(6, 131)
        COD_UnidadLabel.Name = "COD_UnidadLabel"
        COD_UnidadLabel.Size = New System.Drawing.Size(70, 13)
        COD_UnidadLabel.TabIndex = 28
        COD_UnidadLabel.Text = "COD Unidad:"
        '
        'Numero_MatriculaLabel
        '
        Numero_MatriculaLabel.AutoSize = True
        Numero_MatriculaLabel.Location = New System.Drawing.Point(6, 174)
        Numero_MatriculaLabel.Name = "Numero_MatriculaLabel"
        Numero_MatriculaLabel.Size = New System.Drawing.Size(53, 13)
        Numero_MatriculaLabel.TabIndex = 29
        Numero_MatriculaLabel.Text = "Matricula:"
        '
        'NUmero_AcientosLabel
        '
        NUmero_AcientosLabel.AutoSize = True
        NUmero_AcientosLabel.Location = New System.Drawing.Point(7, 211)
        NUmero_AcientosLabel.Name = "NUmero_AcientosLabel"
        NUmero_AcientosLabel.Size = New System.Drawing.Size(51, 13)
        NUmero_AcientosLabel.TabIndex = 30
        NUmero_AcientosLabel.Text = "Acientos:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.Transparent
        Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Label3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.Location = New System.Drawing.Point(2, 152)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(46, 19)
        Label3.TabIndex = 25
        Label3.Text = "Hora"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.BackColor = System.Drawing.Color.Transparent
        Label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Label4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.Location = New System.Drawing.Point(6, 191)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(56, 19)
        Label4.TabIndex = 26
        Label4.Text = "Fecha"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DestinosBindingSource, "Valor", True))
        Me.Label9.Font = New System.Drawing.Font("Arial", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(49, 227)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 34)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "450"
        '
        'DestinosBindingSource
        '
        Me.DestinosBindingSource.DataMember = "Destinos"
        Me.DestinosBindingSource.DataSource = Me.TaxiDBDataConfig
        '
        'TaxiDBDataConfig
        '
        Me.TaxiDBDataConfig.DataSetName = "TaxiDBDataConfig"
        Me.TaxiDBDataConfig.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DestinosBindingSource, "Destino", True))
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 54)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(190, 20)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "NOMBRE DEL DESTINO"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DestinosBindingSource, "Direccion", True))
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(148, 20)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "NOMBRE OFICINA"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 235)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 24)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Bs"
        '
        'cedula
        '
        Me.cedula.AutoSize = True
        Me.cedula.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cedula.Location = New System.Drawing.Point(83, 23)
        Me.cedula.Name = "cedula"
        Me.cedula.Size = New System.Drawing.Size(73, 18)
        Me.cedula.TabIndex = 13
        Me.cedula.Text = "V241222"
        '
        'apellido
        '
        Me.apellido.AutoSize = True
        Me.apellido.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.apellido.Location = New System.Drawing.Point(83, 48)
        Me.apellido.Name = "apellido"
        Me.apellido.Size = New System.Drawing.Size(73, 18)
        Me.apellido.TabIndex = 14
        Me.apellido.Text = "Ledezma"
        '
        'nombre
        '
        Me.nombre.AutoSize = True
        Me.nombre.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nombre.Location = New System.Drawing.Point(83, 74)
        Me.nombre.Name = "nombre"
        Me.nombre.Size = New System.Drawing.Size(64, 18)
        Me.nombre.TabIndex = 15
        Me.nombre.Text = "Roberto"
        '
        'telefono
        '
        Me.telefono.AutoSize = True
        Me.telefono.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.telefono.Location = New System.Drawing.Point(83, 100)
        Me.telefono.Name = "telefono"
        Me.telefono.Size = New System.Drawing.Size(98, 18)
        Me.telefono.TabIndex = 16
        Me.telefono.Text = "0412190971"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.fecha_actual)
        Me.GroupBox1.Controls.Add(Label4)
        Me.GroupBox1.Controls.Add(Label3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.hora_actual)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(7, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(544, 278)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "COOPERATIVA DE TRANSPORTE UNION ENCARNACION BARLOVENTO ORIENTE R.L"
        '
        'fecha_actual
        '
        Me.fecha_actual.AutoSize = True
        Me.fecha_actual.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fecha_actual.Location = New System.Drawing.Point(2, 207)
        Me.fecha_actual.Name = "fecha_actual"
        Me.fecha_actual.Size = New System.Drawing.Size(184, 18)
        Me.fecha_actual.TabIndex = 27
        Me.fecha_actual.Text = "######################"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.lbl_codcli)
        Me.GroupBox2.Controls.Add(Me.telefono)
        Me.GroupBox2.Controls.Add(Me.nombre)
        Me.GroupBox2.Controls.Add(Me.apellido)
        Me.GroupBox2.Controls.Add(Me.cedula)
        Me.GroupBox2.Controls.Add(CedulaLabel)
        Me.GroupBox2.Controls.Add(TelefonoLabel)
        Me.GroupBox2.Controls.Add(ApellidoLabel)
        Me.GroupBox2.Controls.Add(NombreLabel)
        Me.GroupBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(199, 130)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "CLIENTE"
        '
        'lbl_codcli
        '
        Me.lbl_codcli.AutoSize = True
        Me.lbl_codcli.Location = New System.Drawing.Point(83, 0)
        Me.lbl_codcli.Name = "lbl_codcli"
        Me.lbl_codcli.Size = New System.Drawing.Size(25, 13)
        Me.lbl_codcli.TabIndex = 32
        Me.lbl_codcli.Text = "cod"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(NUmero_AcientosLabel)
        Me.GroupBox3.Controls.Add(Me.NUmero_AcientosTextBox)
        Me.GroupBox3.Controls.Add(Numero_MatriculaLabel)
        Me.GroupBox3.Controls.Add(Me.Numero_MatriculaTextBox)
        Me.GroupBox3.Controls.Add(COD_UnidadLabel)
        Me.GroupBox3.Controls.Add(Me.COD_UnidadTextBox)
        Me.GroupBox3.Controls.Add(Label2)
        Me.GroupBox3.Controls.Add(Label1)
        Me.GroupBox3.Controls.Add(Me.FotoPictureBox)
        Me.GroupBox3.Controls.Add(Me.ComboBox1)
        Me.GroupBox3.Controls.Add(Me.Lista)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Label5)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox3.Location = New System.Drawing.Point(211, 19)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(326, 259)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "DETALLES DEL BOLETO"
        '
        'NUmero_AcientosTextBox
        '
        Me.NUmero_AcientosTextBox.BackColor = System.Drawing.Color.MintCream
        Me.NUmero_AcientosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.unidades, "NUmero_Acientos", True))
        Me.NUmero_AcientosTextBox.Enabled = False
        Me.NUmero_AcientosTextBox.Location = New System.Drawing.Point(9, 224)
        Me.NUmero_AcientosTextBox.Name = "NUmero_AcientosTextBox"
        Me.NUmero_AcientosTextBox.Size = New System.Drawing.Size(141, 20)
        Me.NUmero_AcientosTextBox.TabIndex = 31
        '
        'unidades
        '
        Me.unidades.DataMember = "Unidades"
        Me.unidades.DataSource = Me.TaxiDBDataConfig
        '
        'Numero_MatriculaTextBox
        '
        Me.Numero_MatriculaTextBox.BackColor = System.Drawing.Color.MintCream
        Me.Numero_MatriculaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.unidades, "Numero_Matricula", True))
        Me.Numero_MatriculaTextBox.Enabled = False
        Me.Numero_MatriculaTextBox.Location = New System.Drawing.Point(10, 188)
        Me.Numero_MatriculaTextBox.Name = "Numero_MatriculaTextBox"
        Me.Numero_MatriculaTextBox.Size = New System.Drawing.Size(140, 20)
        Me.Numero_MatriculaTextBox.TabIndex = 30
        '
        'COD_UnidadTextBox
        '
        Me.COD_UnidadTextBox.BackColor = System.Drawing.Color.MintCream
        Me.COD_UnidadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.unidades, "COD_Unidad", True))
        Me.COD_UnidadTextBox.Enabled = False
        Me.COD_UnidadTextBox.Location = New System.Drawing.Point(10, 148)
        Me.COD_UnidadTextBox.Name = "COD_UnidadTextBox"
        Me.COD_UnidadTextBox.Size = New System.Drawing.Size(140, 20)
        Me.COD_UnidadTextBox.TabIndex = 29
        '
        'FotoPictureBox
        '
        Me.FotoPictureBox.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.unidades, "Foto", True))
        Me.FotoPictureBox.Location = New System.Drawing.Point(156, 136)
        Me.FotoPictureBox.Name = "FotoPictureBox"
        Me.FotoPictureBox.Size = New System.Drawing.Size(164, 117)
        Me.FotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.FotoPictureBox.TabIndex = 26
        Me.FotoPictureBox.TabStop = False
        '
        'ComboBox1
        '
        Me.ComboBox1.BackColor = System.Drawing.Color.MintCream
        Me.ComboBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ComboBox1.DataSource = Me.unidades
        Me.ComboBox1.DisplayMember = "Modelo"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(156, 102)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(164, 28)
        Me.ComboBox1.TabIndex = 25
        Me.ComboBox1.ValueMember = "COD_Unidad"
        '
        'Lista
        '
        Me.Lista.BackColor = System.Drawing.Color.MintCream
        Me.Lista.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Lista.DataSource = Me.DestinosBindingSource
        Me.Lista.DisplayMember = "Destino"
        Me.Lista.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lista.FormattingEnabled = True
        Me.Lista.Location = New System.Drawing.Point(156, 19)
        Me.Lista.Name = "Lista"
        Me.Lista.Size = New System.Drawing.Size(164, 28)
        Me.Lista.TabIndex = 22
        Me.Lista.ValueMember = "Cod_Destino"
        '
        'hora_actual
        '
        Me.hora_actual.AutoSize = True
        Me.hora_actual.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hora_actual.Location = New System.Drawing.Point(1, 171)
        Me.hora_actual.Name = "hora_actual"
        Me.hora_actual.Size = New System.Drawing.Size(80, 18)
        Me.hora_actual.TabIndex = 24
        Me.hora_actual.Text = "#########"
        '
        'DestinosTableAdapter
        '
        Me.DestinosTableAdapter.ClearBeforeFill = True
        '
        'hora
        '
        Me.hora.Enabled = True
        Me.hora.Interval = 1000
        '
        'vender
        '
        Me.vender.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.vender.ForeColor = System.Drawing.SystemColors.ControlText
        Me.vender.Location = New System.Drawing.Point(465, 296)
        Me.vender.Name = "vender"
        Me.vender.Size = New System.Drawing.Size(79, 24)
        Me.vender.TabIndex = 24
        Me.vender.Text = "CONFIRMAR"
        Me.vender.UseVisualStyleBackColor = False
        '
        'cancel
        '
        Me.cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cancel.Location = New System.Drawing.Point(7, 296)
        Me.cancel.Name = "cancel"
        Me.cancel.Size = New System.Drawing.Size(79, 24)
        Me.cancel.TabIndex = 25
        Me.cancel.Text = "CANCELAR"
        Me.cancel.UseVisualStyleBackColor = False
        '
        'progreso
        '
        Me.progreso.ForeColor = System.Drawing.SystemColors.ControlText
        Me.progreso.Location = New System.Drawing.Point(92, 296)
        Me.progreso.Maximum = 2000
        Me.progreso.Name = "progreso"
        Me.progreso.Size = New System.Drawing.Size(367, 24)
        Me.progreso.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progreso.TabIndex = 17
        Me.progreso.Value = 1
        Me.progreso.Visible = False
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.BoletoTableAdapter = Nothing
        Me.TableAdapterManager.ClienteTableAdapter = Nothing
        Me.TableAdapterManager.DestinosTableAdapter = Me.DestinosTableAdapter
        Me.TableAdapterManager.UnidadesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = SistemaDeAutomatizacion.TaxiDBDataConfigTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'BoletoTableAdapter1
        '
        Me.BoletoTableAdapter1.ClearBeforeFill = True
        '
        'UnidadesTableAdapter
        '
        Me.UnidadesTableAdapter.ClearBeforeFill = True
        '
        'frmVentas
        '
        Me.AcceptButton = Me.vender
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PaleGreen
        Me.CancelButton = Me.cancel
        Me.ClientSize = New System.Drawing.Size(556, 325)
        Me.Controls.Add(Me.progreso)
        Me.Controls.Add(Me.cancel)
        Me.Controls.Add(Me.vender)
        Me.Controls.Add(Me.GroupBox1)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Venta de  pasajes"
        Me.TopMost = True
        CType(Me.DestinosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TaxiDBDataConfig, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.unidades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FotoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Lista As ComboBox
    Friend WithEvents TaxiDBDataConfig As TaxiDBDataConfig
    Friend WithEvents DestinosBindingSource As BindingSource
    Friend WithEvents DestinosTableAdapter As TaxiDBDataConfigTableAdapters.DestinosTableAdapter
    Public WithEvents hora As Timer
    Friend WithEvents vender As Button
    Friend WithEvents cancel As Button
    Friend WithEvents progreso As ProgressBar
    Friend WithEvents cedula As Label
    Friend WithEvents apellido As Label
    Friend WithEvents nombre As Label
    Friend WithEvents telefono As Label
    Friend WithEvents hora_actual As Label
    Friend WithEvents TableAdapterManager As TaxiDBDataConfigTableAdapters.TableAdapterManager
    Friend WithEvents BoletoTableAdapter1 As TaxiDBDataConfigTableAdapters.BoletoTableAdapter
    Friend WithEvents Label10 As Label
    Friend WithEvents unidades As BindingSource
    Friend WithEvents UnidadesTableAdapter As TaxiDBDataConfigTableAdapters.UnidadesTableAdapter
    Friend WithEvents FotoPictureBox As PictureBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents NUmero_AcientosTextBox As TextBox
    Friend WithEvents Numero_MatriculaTextBox As TextBox
    Friend WithEvents COD_UnidadTextBox As TextBox
    Friend WithEvents lbl_codcli As Label
    Friend WithEvents fecha_actual As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label9 As Label
End Class
