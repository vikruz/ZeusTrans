﻿Imports System.ComponentModel
Public Class frmVentas
    Dim nombre_cliente As String
    Dim apellido_cliente As String
    Dim cedula_cliente As String
    Dim telefono_cliente As String
    Dim codigo_cliente As String
    Private Sub frmVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataConfig.Unidades' Puede moverla o quitarla según sea necesario.
        Me.UnidadesTableAdapter.Fill(Me.TaxiDBDataConfig.Unidades)
        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataConfig.Destinos' Puede moverla o quitarla según sea necesario.
        Me.DestinosTableAdapter.Fill(Me.TaxiDBDataConfig.Destinos)
        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataConfig.Boleto' Puede moverla o quitarla según sea necesario.
        Me.DestinosTableAdapter.Fill(Me.TaxiDBDataConfig.Destinos)
    End Sub
    Sub New(ByVal nombre_cliente As String, ByVal apellido_cliente As String, ByVal cedula_cliente As String, ByVal telefono_cliente As String, ByVal codigo_cliente As String)
        'En este sub lo que hago es capturar los parametros que paso desde el formulario de clientes 
        InitializeComponent() 'es necesario que lleve esta linea para que inicialice los componentes y las variables dentro del form  
        cedula.Text = cedula_cliente
        apellido.Text = apellido_cliente
        nombre.Text = nombre_cliente
        telefono.Text = telefono_cliente
        lbl_codcli.Text = codigo_cliente
    End Sub
    Private Sub FillByToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            Me.DestinosTableAdapter.FillBy(Me.TaxiDBDataConfig.Destinos)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AgregarTexto(text As String, size As Double, xpos As Integer, ypos As Integer)
        Dim lbl As New Label
        lbl.AutoSize = True
        lbl.Font = New Font("Arial", size, FontStyle.Regular, GraphicsUnit.Pixel)
        lbl.Location = New Point(xpos, ypos)
        lbl.Text = text
        Me.Controls.Add(lbl)
    End Sub
    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click
        AgregarTexto(System.DateTime.Now, 12, 50, 50)
    End Sub
    Sub capturar()
        ' Capturar todo el área del formulario
        Dim gr As Graphics = Me.CreateGraphics
        ' Tamaño de lo que queremos copiar
        Dim fSize As Size = Me.Size
        ' Creamos el bitmap con el área que vamos a capturar
        ' En este caso, con el tamaño del formulario actual
        Dim bm As New Bitmap(fSize.Width, fSize.Height, gr)
        ' Un objeto Graphics a partir del bitmap
        Dim gr2 As Graphics = Graphics.FromImage(bm)
        ' Copiar el área de la pantalla que ocupa el formulario
        gr2.CopyFromScreen(Me.Location.X, Me.Location.Y, 0, 0, fSize)
        ' Asignamos la imagen al PictureBox
        'Me.picCaptura.Image = bm
    End Sub
    Private Sub cancel_Click(sender As Object, e As EventArgs) Handles cancel.Click
        Me.Close()
    End Sub
    Private Sub vender_Click(sender As Object, e As EventArgs) Handles vender.Click
        vender.Enabled = False
        progreso.Visible = True
        progreso.Value = 0
        Try
            Me.TopMost = False
            For i = 0 To 1999
                progreso.Value = progreso.Value + 1
            Next
            Me.BoletoTableAdapter1.INSERTAR(lbl_codcli.Text, fecha_actual.Text, Lista.SelectedValue, hora_actual.Text, COD_UnidadTextBox.Text)
            Dim boleto As New BoletoFormato(nombre.Text, apellido.Text, cedula.Text, hora_actual.Text, fecha_actual.Text,
                                            Label6.Text, Label7.Text, Label9.Text, COD_UnidadTextBox.Text, Numero_MatriculaTextBox.Text)
            boleto.ShowDialog(Me)
            MsgBox("Venta finalizada!", vbInformation, "Confirmado!")
        Catch ex As Exception
            MsgBox("No se pudo vender el boleto, contacte con el programador!" & vbCrLf & ex.Message, vbCritical)
        Finally
            vender.Enabled = True
            progreso.Visible = False
            Me.TopMost = True
        End Try
        Me.Close()
    End Sub
    Private Sub hora_Tick(sender As Object, e As EventArgs) Handles hora.Tick
        hora_actual.Text = System.DateTime.Now.ToShortTimeString
        fecha_actual.Text = System.DateTime.Now.ToLongDateString
    End Sub
    Private Sub Label10_Click(sender As Object, e As EventArgs) Handles Label10.Click
        '  Label10.Text = Lista.SelectedValue
    End Sub
    Private Sub frmVentas_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        FrmClientes.ActiveForm.Opacity = 100
    End Sub

End Class