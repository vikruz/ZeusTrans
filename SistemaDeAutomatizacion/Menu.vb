﻿
Imports System.ComponentModel
Imports System.Data
Imports System.Data.OleDb

Public Class MENUfrm

    Private Sub status_Tick(sender As Object, e As EventArgs) Handles status.Tick
        '  Dim textoa As String
        '  textoa = My.Computer.Name & vbNewLine & My.User.Name & My.Computer.Info.OSFullName.ToString & vbNewLine &
        '  My.Computer.Info.OSPlatform.ToString & vbNewLine &
        '  My.Computer.Info.OSVersion.ToString & vbNewLine & "Memoria Fisica " & FormatNumber(My.Computer.Info.TotalPhysicalMemory, 0) & " bytes" & vbNewLine & "Memoria Virtual " & FormatNumber(My.Computer.Info.TotalVirtualMemory, 0) & " bytes"
        '   AgregarTexto(textoa, 10, 1, 480)
        tiempo.Text = System.DateTime.Now.ToLongTimeString
        fecha.Text = System.DateTime.Now.ToLongDateString

    End Sub
    Private Sub AgregarTexto(text As String, size As Double, xpos As Integer, ypos As Integer)
        Dim lbl As New Label
        lbl.AutoSize = True
        lbl.Font = New Font("Arial", size, FontStyle.Regular, GraphicsUnit.Pixel)
        lbl.Location = New Point(xpos, ypos)
        lbl.Text = text
        Me.Controls.Add(lbl)
    End Sub
    Private Sub Pasajes_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        InicioPrincipal.Show()
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        NotifyIcon1.Visible = False
        Me.Show()
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub Pasajes_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            NotifyIcon1.Visible = True
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info
            NotifyIcon1.BalloonTipTitle = "Sistema de automatizacion TaxiCoop"
            NotifyIcon1.BalloonTipText = "La aplicacion sigue ejecutandose en segundo plano " & vbNewLine & "Doble Click Para restaurar la ventana de pasajes"
            NotifyIcon1.ShowBalloonTip(300)
            Me.Hide()
        End If
        If Me.WindowState = FormWindowState.Normal Then
            NotifyIcon1.Visible = False
            Me.Show()
        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub ClientesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ClientesToolStripMenuItem1.Click


        Dim clientes As New FrmClientes
        clientes.MdiParent = Me
        clientes.Show()

    End Sub

    Private Sub ConductoresToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ConductoresToolStripMenuItem1.Click

        Dim unidades As New Unidades
        unidades.MdiParent = Me
        unidades.Show()

    End Sub


    Private Sub MENUfrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub ControlDeVentasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ControlDeVentasToolStripMenuItem.Click
        Dim tabla_boletos As New tabla_ventas
        tabla_boletos.MdiParent = Me
        tabla_boletos.Show()

    End Sub

    Private Sub MENUfrm_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If MsgBox("Cerrar sesión?", vbQuestion + vbYesNo, "Sistema") = vbYes Then
            e.Cancel = False
        Else
            e.Cancel = True
        End If

    End Sub

    Private Sub CerrarSesiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarSesiónToolStripMenuItem.Click
        Me.Close()
    End Sub

End Class