﻿
Imports System.ComponentModel
Imports System.Data
Imports System.Data.OleDb
Public Class Pasajes
    Private Sub CANCELAR_Click(sender As Object, e As EventArgs) Handles CANCELAR.Click
        InicioPrincipal.Show()
        Me.Close()
    End Sub

    Private Sub status_Tick(sender As Object, e As EventArgs) Handles status.Tick
        Dim textoa As String
        textoa = My.Computer.Name & vbNewLine & My.User.Name & My.Computer.Info.OSFullName.ToString & vbNewLine &
        My.Computer.Info.OSPlatform.ToString & vbNewLine &
        My.Computer.Info.OSVersion.ToString & vbNewLine & "Memoria Fisica " & FormatNumber(My.Computer.Info.TotalPhysicalMemory, 0) & " bytes" & vbNewLine & "Memoria Virtual " & FormatNumber(My.Computer.Info.TotalVirtualMemory, 0) & " bytes"
        AgregarTexto(textoa, 10, 1, 480)
    End Sub
    Private Sub AgregarTexto(text As String, size As Double, xpos As Integer, ypos As Integer)
        Dim lbl As New Label
        lbl.AutoSize = True
        lbl.Font = New Font("Arial", size, FontStyle.Regular, GraphicsUnit.Pixel)
        lbl.Location = New Point(xpos, ypos)
        lbl.Text = text
        Me.Controls.Add(lbl)
    End Sub
    Private Sub Pasajes_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        InicioPrincipal.Show()
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        NotifyIcon1.Visible = False
        Me.Show()
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub Pasajes_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            NotifyIcon1.Visible = True
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info
            NotifyIcon1.BalloonTipTitle = "Sistema de automatizacion TaxiCoop"
            NotifyIcon1.BalloonTipText = "La aplicacion sigue ejecutandose en segundo plano " & vbNewLine & "Doble Click Para restaurar la ventana de pasajes"
            NotifyIcon1.ShowBalloonTip(300)
            Me.Hide()
        End If
        If Me.WindowState = FormWindowState.Normal Then
            NotifyIcon1.Visible = False
            Me.Show()
        End If
    End Sub

    Private Sub Pasajes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DATADataSet1.Clientes' Puede moverla o quitarla según sea necesario.
        Me.ClientesTableAdapter.Fill(Me.DATADataSet.Clientes)

        'TODO: esta línea de código carga datos en la tabla 'DATADataSet.Clientes' Puede moverla o quitarla según sea necesario.
        Me.ClientesTableAdapter.Fill(Me.DATADataSet.Clientes)
        'TODO: esta línea de código carga datos en la tabla 'DATADataSet.Clientes' Puede moverla o quitarla según sea necesario.
        Me.ClientesTableAdapter.Fill(Me.DATADataSet.Clientes)
        'TODO: esta línea de código carga datos en la tabla 'DATADataSet.Clientes' Puede moverla o quitarla según sea necesario.
        Me.ClientesTableAdapter.Fill(Me.DATADataSet.Clientes)

    End Sub

    Private Sub ClientesBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.ClientesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DATADataSet)

    End Sub

    Private Sub ClientesBindingNavigatorSaveItem_Click_1(sender As Object, e As EventArgs)
        Me.Validate()
        Me.ClientesBindingSource.EndEdit()

        Me.TableAdapterManager.UpdateAll(Me.DATADataSet)

    End Sub

    Private Sub ClientesBindingNavigatorSaveItem_Click_2(sender As Object, e As EventArgs) Handles ClientesBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.ClientesBindingSource.EndEdit()

    End Sub



    Private Sub BindingNavigatorAddNewItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorAddNewItem.Click
        Me.ClientesBindingSource.AddNew()
    End Sub

    Private Sub ClientesBindingSource_AddingNew(sender As Object, e As AddingNewEventArgs) Handles ClientesBindingSource.AddingNew

    End Sub
End Class