﻿Public Class Unidades
    Private Sub ConductorBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles ConductorBindingNavigatorSaveItem.Click
        Me.Validate()

    End Sub

    Private Sub Conductores_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataConfig.Unidades' Puede moverla o quitarla según sea necesario.
        Me.UnidadesTableAdapter.Fill(Me.TaxiDBDataConfig.Unidades)
        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataSet.Boleto' Puede moverla o quitarla según sea necesario.
        'HABILITARLUEGO'    Me.BoletoTableAdapter.Fill(Me.TaxiDBDataSet.Boleto)
        'TODO: esta línea de código carga datos en la tabla 'TaxiDBDataSet.Conductor' Puede moverla o quitarla según sea necesario.
        '  Me.ConductorTableAdapter.Fill(Me.TaxiDBDataSet.Conductor)

    End Sub

    Private Sub UnidadesBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)

        Me.Validate()
        Me.UnidadesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.TaxiDBDataConfig)

    End Sub

End Class